#!/system/bin/sh

BUSYBOX=/system/xbin/busybox

system_ro=0

# make sure we're running as root
check_root()
{
	if [ "$($BUSYBOX id -u)" != "0" ]; then
		if [ "$($BUSYBOX id -g)" != "0" ]; then
			echo "Must be root!"
			exit 1
		fi
	fi
}

do_exit()
{
    if [[ $system_ro == 1 ]]; then
        $BUSYBOX mount -o remount,ro /system
    fi
    exit $1
}

install_binaries()
{
    check_root
    if [[ "$($BUSYBOX mount | $BUSYBOX grep '/system' | $BUSYBOX grep ro)" != "" ]]; then
        system_ro=1
        $BUSYBOX mount -o remount,rw /system
    fi
    $BUSYBOX mkdir -p /system/xbin || do_exit 1
    $BUSYBOX cp busybox /system/xbin/securesd-bb || do_exit 1
    $BUSYBOX cp cryptsetup /system/xbin/securesd-cryptsetup || do_exit 1
    $BUSYBOX cp securesd /system/xbin/securesd || do_exit 1
    $BUSYBOX chmod 6755 /system/xbin/securesd || do_exit 1
    do_exit 0
}

